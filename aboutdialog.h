#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

#include <QDialog>

#include <QPainter>

#include <QRect>
#include <QMessageBox>
#include <QScrollBar>
#include <QSlider>

namespace Ui {
class AboutDialog;
}

class AboutDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AboutDialog(QWidget *parent = 0);
    ~AboutDialog();

public:
    //处理窗口重绘，实现背景图片
    void paintEvent(QPaintEvent *event);


private slots:
    void on_pushButton_closeW_clicked();

private:
    Ui::AboutDialog *ui;
};

#endif // ABOUTDIALOG_H
