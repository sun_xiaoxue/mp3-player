#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);

    //去掉窗口标题
    this->setWindowFlag(Qt::FramelessWindowHint);
    //禁止改变窗口尺寸大小
    this->setFixedSize(this->width(),this->height());
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
void AboutDialog::paintEvent(QPaintEvent *event){
    QPainter qPainter(this);

    //调用drawPixmap()函数进行绘图
    qPainter.drawPixmap(0,0,width(),height(),QPixmap(":/new/prefix1/MBGMusicplayer_ui5.jpg"));
}

void AboutDialog::on_pushButton_closeW_clicked()
{
    this->close();
}


