#include "widget.h"
#include "ui_widget.h"

#pragma excution_character_set("utf-8")

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    //去掉窗口标题
    this->setWindowFlag(Qt::FramelessWindowHint);
    this->setWindowIcon(QIcon(":/new/prefix1/guitar.png"));

    //horizontalSlider_PlayProgress = new QSlider(Qt::Horizontal,this);
    //horizontalSlider_PlayProgress->setGeometry(8,320,375,25);

    //播放进度条控件美化
    ui->horizontalSlider_PlayProgress->setStyleSheet(
                    "QSlider::groove:horizontal {"
                    "border:1px solid pink;"
                    "background-color:pink;"
                    "height:10px;"
                    "border-radius:5px;"
                    "}"

                    "QSlider::handle:horizontal {"
                    "background:qradialgradient(spread:pad,cx:0.5,cy:0.5,radius:0.5,fx:0.5,fy:0.5,stop:0.7 white,stop:0.8 rgb(140,212,255));"
                    "width:20px;"
                    "border-radius:10px;"
                    "margin-top:-5px;"
                    "margin-bottom:-5px;}"

                    "horizontalSlider::sub-page:horizontal{"
                    "background:red;"
                    "margin:5px;"
                    "border-radius:5px;}"
                    );

      ui->horizontalSlider_Volume->setStyleSheet(
                    "QSlider::groove:horizontal {"
                    "border:1px solid skyblue;"
                    "background-color:skyblue;"
                    "height:10px;"
                    "border-radius:5px;"
                    "}"

                    "QSlider::handle:horizontal {"
                    "background:qradialgradient(spread:pad,cx:0.5,cy:0.5,radius:0.5,fx:0.5,fy:0.5,stop:0.7 white,stop:0.8 rgb(140,212,255));"
                    "width:20px;"
                    "border-radius:10px;"
                    "margin-top:-5px;"
                    "margin-bottom:-5px;}"

                    "horizontalSlider::sub-page:horizontal{"
                    "background:red;"
                    "margin:5px;"
                    "border-radius:5px;}"
                    );

      //将光标焦点定位到搜索框控件中
      ui->lineEdit_InputSongs->setFocus();

      //初始化QNetworkAccessManager的一个实例
      NetworkAccessManagers = new QNetworkAccessManager(this);

      iPlayFlags=0;
      //初始化文本对象
      docTextObject = ui->plainTextEdit_Songlist->document();
      //将plainTextEdit_Songlist设置为只读模式
      ui->plainTextEdit_Songlist->setReadOnly(true);

      //初始化多媒体实例
      P_PlayerObjects=new QMediaPlayer(this);
      P_PlayerLists=new QMediaPlaylist(this);
      //设置播放列表
      P_PlayerObjects->setMedia(P_PlayerLists);
      //设置播放模式
      P_PlayerLists->setPlaybackMode(QMediaPlaylist::Loop);

      //信号与槽连接
     // connect(P_PlayerObjects,SIGNAL(positionChanged(qint64)),this,SLOT(HandleLCDNumberTimeChangeFunc(qint64)));
      connect(P_PlayerObjects,SIGNAL(durationChanged(qint64)),this,SLOT(HandleProgressTimeChangeFunc(qint64)));
      connect(P_PlayerObjects,SIGNAL(positionChanged(qint64)),this,SLOT(HandlePostionChangeFunc(qint64)));
      connect(NetworkAccessManagers,SIGNAL(finished(QNetworkReply*)),this,SLOT(HandleDataBackFunc(QNetworkReply*)));

      //处理标题显示歌曲信息动画字幕
      ui->label_DisplaySongTitle->setFont(QFont("宋体",14,QFont::Normal));
      strTitles=ui->label_DisplaySongTitle->text();
      //使用计时器来移动标题
      QTimer *p_MoveTitle = new QTimer(this);
      iTitleChar = 0;
      connect(p_MoveTitle,SIGNAL(timeout()),this,SLOT(titleMoveCaptionChar()));
      p_MoveTitle->start(1000); //1000ms启动一次
}

Widget::~Widget()
{
    delete ui;
}
//处理窗口重绘背景图片
void Widget::paintEvent(QPaintEvent *event){
    QPainter qPainter(this);

    //调用drawPixmap()函数进行绘图
    qPainter.drawPixmap(0,0,width(),height(),QPixmap(":/new/prefix1/MBGMusicplayer_ui4.jpg"));

}
//功能：使用鼠标拖动窗口移动MP3播放器到任意位置
void Widget::mousePressEvent(QMouseEvent *event){
    if(event->button() == Qt::LeftButton){
        m_MouseDrag = true;
        mouseStartPoint = event->globalPos();
        windowStartPoint=this->frameGeometry().topLeft();
    }
}
void Widget::mouseMoveEvent(QMouseEvent *event){
    if(m_MouseDrag){
        //获取鼠标移动的距离
        QPoint m_Distance=event->globalPos()-mouseStartPoint;

        //更改窗口的对应位置
        this->move(windowStartPoint+m_Distance);
    }

}
void Widget::mouseReleaseEvent(QMouseEvent *event){
    //释放鼠标事件（左键）
    if(event->button() == Qt::LeftButton){
        m_MouseDrag = false;
    }
}

//关闭窗口
void Widget::on_pushButton_exit_clicked()
{
    this->close();
}

void Widget::on_pushButton_about_clicked()
{
    //QMessageBox::information(this,"提示","关于对话框",QMessageBox::Yes);
    AboutDialog *pAboutDialog = new AboutDialog(this);

    QRect qRect=geometry();
    int iX=qRect.x()+80; //偏移值：高度差/2
    int iY=qRect.y()+80;  //偏移值：宽度差/2   美观化，取120
    pAboutDialog->setGeometry(iX,iY,520,409); // 364 220

    //执行模态对话框
    pAboutDialog->exec(); //exec()会阻塞其他的窗口   show（）显示的窗口能够任意拖动

}

void Widget::on_pushButton_AddSong_clicked()
{
    QString strCurrentPaths = QDir::homePath();
    QString strDlgTitle = "请选择MP3音乐文件";
    QString strFileters = "所有文件(*.*);;音频文件(*.mp3);;mp3文件(*.mp3)";

    QStringList strMp3FileList = QFileDialog::getOpenFileNames(this,strDlgTitle,strCurrentPaths,strFileters);

    for(int i=0;i<strMp3FileList.count();i++)
    {
        QString strFileters = strMp3FileList.at(i);

        //将音乐文件添加到播放列表
        P_PlayerLists->addMedia(QUrl::fromLocalFile(strFileters));
        QFileInfo qFileInfo(strFileters);

        //将歌曲添加到plainTextEdit控件
        ui->plainTextEdit_Songlist->appendPlainText(qFileInfo.fileName());
    }
    //添加完毕立刻播放
    if(P_PlayerObjects->state() != QMediaPlayer::PlayingState)
    {
        P_PlayerLists->setCurrentIndex(0);     //添加完成后，立刻播放，默认播放第一首
    }
    P_PlayerObjects->play();

}


void Widget::on_pushButton_play_clicked()
{
    //如果是停止状态或暂停状态，则播放
    if(iPlayFlags == 0){
        P_PlayerObjects->play();
        iPlayFlags=1;
    }else if(P_PlayerLists->currentIndex()<0){
        P_PlayerLists->setCurrentIndex(0);
    }
    P_PlayerObjects->play();
}

void Widget::on_pushButton_pasue_clicked()
{
    //如果是播放状态，则暂停
    if(iPlayFlags=1){
        P_PlayerObjects->pause();
        iPlayFlags=0;
    }
}

//停止播放
void Widget::on_pushButton_stop_clicked()
{
    P_PlayerObjects->stop();
}

void Widget::on_pushButton_previous_clicked()
{
    int m_index=P_PlayerLists->currentIndex(); //先获取当前歌曲
    int m_indexsum = P_PlayerLists->mediaCount();//获取歌曲总数

    if(m_index>0){
        m_index=m_index-1;
        P_PlayerLists->setCurrentIndex(m_index);
        P_PlayerObjects->play();
    }else{
        m_index=m_indexsum-1; //else的情况是m_index=0的情况，它的上一曲就是列表中所有歌曲的最后一曲
        P_PlayerLists->setCurrentIndex(m_index);
        P_PlayerObjects->play();
    }

}

void Widget::on_pushButton_next_clicked()
{
    int m_index=P_PlayerLists->currentIndex(); //先获取当前歌曲
    int m_indexsum = P_PlayerLists->mediaCount();//获取歌曲总数

    if(m_index<m_indexsum){
        m_index=m_index+1;
        P_PlayerLists->setCurrentIndex(m_index);
        P_PlayerObjects->play();
    }else{
        m_index=0;
        P_PlayerLists->setCurrentIndex(m_index);
        P_PlayerObjects->play();
    }
}

//实现静音状态，图标之间切换问题
void Widget::on_pushButton_mute_clicked()
{
    bool bMutexState = P_PlayerObjects->isMuted();
    P_PlayerObjects->setMuted(!bMutexState);

    if(bMutexState){
        ui->pushButton_mute->setIcon(QIcon(":/new/prefix1/MSound.png"));
    }else{
        ui->pushButton_mute->setIcon(QIcon(":/new/prefix1/MNoSound.png"));
    }

}

//处理数据信息返回函数
void Widget::HandleDataBackFunc(QNetworkReply *pReply)
{
    //读取网络回馈数据
    QByteArySearchInfo = pReply->readAll();
    QJsonParseError JSonPError;//定义错误信息对象

    //将json文本转换为json文件对象
    QJsonDocument JSonDoc_RecvData=QJsonDocument::fromJson(QByteArySearchInfo,&JSonPError);
    if(JSonPError.error != QJsonParseError::NoError){ //判断是否符合规则
        QMessageBox::critical(this,"Prompt","提示:搜索歌曲获取json格式错误，请重新检查",QMessageBox::Yes);
        return;
    }

    //QJsonObject使用函数object()
    QJsonObject TotalJsonObject=JSonDoc_RecvData.object();
    //列出json里面所有的key
    QStringList strKeys=TotalJsonObject.keys();

    if(strKeys.contains("result")){ //有数据信息
        //将带有result的数据内容提取之后转换为对象
        QJsonObject resultobject=TotalJsonObject["result"].toObject();

        //存储所有keys
        QStringList strResultKeys = resultobject.keys();
        /*
        if(strResultKeys.contains("songCount")){
            int songCount;


        } */
        //如果keys是songs，证明搜索到了对应的歌曲，可以参考官网提供的API
        if(strResultKeys.contains("songs")){
            QJsonArray array = resultobject["songs"].toArray();

            //通过for循环查找歌曲当中的字段信息
            for(auto isong:array){ //疑问点：for循环，auto
                QJsonObject jsonobject1 = isong.toObject();

                //获取歌曲ID  歌曲名称artists  歌手
                I_MusicID = jsonobject1["id"].toInt();
                StrMusicName = jsonobject1["name"].toString();

                QStringList strKeys=jsonobject1.keys();

                if(strKeys.contains("artists")){
                    QJsonArray artistsjsonarray = jsonobject1["artists"].toArray();
                    for(auto js:artistsjsonarray){
                        QJsonObject jsonobject2 = js.toObject();
                        StrSingerName=jsonobject2["name"].toString();
                    }

                }
            }
        }
        // 增加 歌曲具体信息到media
        QString strURL;
        strURL = QString("https://music.163.com/song/media/outer/url?id=%0").arg(I_MusicID);//id=%0 获取到的是I_MusicID

        P_PlayerLists->addMedia(QUrl(strURL));
        ui->plainTextEdit_Songlist->appendPlainText(StrMusicName+"_"+StrSingerName);

        //歌曲更新到media完毕立刻播放
        if(P_PlayerObjects->state() != QMediaPlayer::PlayingState)
        {
            P_PlayerLists->setCurrentIndex(0);     //添加完成后，立刻播放，默认播放第一首
        }
        P_PlayerObjects->play();
    }

}
//处理LCDNumber控件时间变化
/*
void Widget::HandleLCDNumberTimeChangeFunc(qint64 duration)
{

    int int_Second=duration/1000;
    int int_Minute=int_Second/60;
    int_Second=int_Second%60;

    strSongTime=QString::asprintf("%d:%d",int_Minute,int_Second);
    ui->lcdNumber->display(strSongTime);


}
*/
//处理进度条控件变化
void Widget::HandleProgressTimeChangeFunc(qint64 duration)//duration持续改变
{
    // 设置进度条最大值
        ui->horizontalSlider_PlayProgress->setMaximum(duration);


        int secs = duration/1000;
        int mins = secs/60;
        secs = secs%60;
        durationTime = QString::asprintf("%d:%d",mins,secs);
        ui->label_time->setText(positionTime+"/"+durationTime);


        //设置滚动字幕
        int int_Value = P_PlayerLists->currentIndex();
        QTextBlock qtextblocks=docTextObject->findBlockByNumber(int_Value);
        QString strTitle=qtextblocks.text();
        ui->label_DisplaySongTitle->setText(strTitle);

}
//歌曲播放位置变化
void Widget::HandlePostionChangeFunc(qint64 position)
{

    if(ui->horizontalSlider_PlayProgress->isSliderDown())  //如果手动调整进度条，则不处理
        return;

    //设置滑块位置
    ui->horizontalSlider_PlayProgress->setSliderPosition(position);


    int secs = position/1000;
    int mins = secs/60;
    secs = secs % 60;
    positionTime = QString::asprintf("%d:%d",mins,secs);
    ui->label_time->setText(positionTime+"/"+durationTime);


}

//实现字幕动态变化
void Widget::titleMoveCaptionChar(){
    strTitles=ui->label_DisplaySongTitle->text();

        if(iTitleChar<strTitles.length())
        {
            QString strTemp=strTitles.mid(iTitleChar)+strTitles.mid(0,iTitleChar);
            ui->label_DisplaySongTitle->setText(strTemp);
            iTitleChar++;
        }
        else
            iTitleChar=0;

}


//搜索歌曲名称的实现
void Widget::on_pushButton_SearchSong_clicked()
{
    QString str1,str2;
    str1 = ui->lineEdit_InputSongs->text();
    
    str2 = "http://music.163.com/api/search/get/web?csrf_token=hlpretag=&hlposttag=&s={"+str1+"}&type=1&offset=0&total=true&limit=1";

    //定义一个QNetworkRequest请求
    QNetworkRequest networkRequest;

    //将请求格式设置给对应请求对象
    networkRequest.setUrl(str2);

    //使用QNetworkAccessManger类对应的API发送GET请求并获取响应数据
    NetworkAccessManagers->get(networkRequest); //请求处理
    
}

//音量控制
void Widget::on_horizontalSlider_Volume_valueChanged(int value)
{
    P_PlayerObjects->setVolume(value);
}
//播放歌曲进度控制
/*
void Widget::on_horizontalSlider_PlayProgress_valueChanged(int value)
{
    P_PlayerObjects->setPosition(value);
}

void Widget::on_horizontalSlider_PlayProgress_sliderMoved(int position)
{

    if(qAbs(P_PlayerObjects->position()-position)>99)
    {
        P_PlayerObjects->setPosition(position);
    }

    //P_PlayerObjects->setPosition(position);
}
*/


void Widget::on_horizontalSlider_PlayProgress_sliderReleased()
{
    P_PlayerObjects->setPosition(ui->horizontalSlider_PlayProgress->value());
}
