#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "aboutdialog.h"
#include "warning.h"
#include <QPainter>
#include <QRect>
#include <QMessageBox>
#include <QScrollBar>
#include <QSlider>

//网络编程头文件
//QNetworkAccessManger,使用该类可以进行网络请求、获取响应、处理错误等操作
#include <QNetworkAccessManager>

//<QNetworkReply>是QNetworkAccessManger发送请求之后返回的响应类
#include <QNetworkReply>

//添加多媒体播放器类
#include <QtMultimedia>
#include <QMediaPlayer>

//添加多媒体播放列表类
#include <QMediaPlayer>
#include <QMediaPlaylist>

#include <QTextBlock>
#include <QTimer>
#include <QFileDialog>

//用于解析JSON数据时错误处理类
#include <QJsonParseError>
//用于操作JSON数组的类
#include <QJsonObject>
#include <QJsonArray>

#include <QLCDNumber>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
public:
      //处理窗口重新绘制背景图片
      void paintEvent(QPaintEvent *event);

      //功能：使用鼠标拖动窗口移动MP3播放器到桌面的任意位置
      void mousePressEvent(QMouseEvent *event);
      void mouseMoveEvent(QMouseEvent *event);
      void mouseReleaseEvent(QMouseEvent *event);
      bool m_MouseDrag; //鼠标是否按下
      QPoint mouseStartPoint;//获取鼠标的起始位置
      QPoint windowStartPoint;//获取窗口的起始位置

      //2024/3/16自定义成员如下：
      QMediaPlayer *P_PlayerObjects;//定义多媒体播放器对象
      QMediaPlaylist *P_PlayerLists; //定义多媒体播放列表

      QString StrLocalMusicPath; //导入本地音乐路径

      //歌曲ID
      int I_MusicID;

      //歌曲名称 歌手姓名
      QString StrMusicName,StrSingerName;

      int iPlayFlags; //标记播放状态

      QByteArray QByteArySearchInfo; //存储搜索数据信息
      QTextDocument *docTextObject; //处理富文本内容
      QTextBlock *qTextLine; //处理文本块指针

      QNetworkAccessManager *NetworkAccessManagers;

      //处理动态字幕
      QString strTitles;
      int iTitleChar;

      //文件显示时长
      QString durationTime;
      QString positionTime;
      //QString strSongTime;
     // 定义水平进度条
      QSlider *slider;


      //自定义槽函数
public slots:
      //处理数据信息返回函数
      void HandleDataBackFunc(QNetworkReply *pReply);
      //处理LCDNumber控件时间变化
      //void HandleLCDNumberTimeChangeFunc(qint64 duration);
      //处理进度条控件变化
      void HandleProgressTimeChangeFunc(qint64 duration);
      //处理播放位置变化
      void HandlePostionChangeFunc(qint64 position);

      //实现字幕动态变化
      void titleMoveCaptionChar();

private slots:
      void on_pushButton_AddSong_clicked();

      void on_pushButton_play_clicked();

      void on_pushButton_pasue_clicked();

      void on_pushButton_stop_clicked();

      void on_pushButton_previous_clicked();

      void on_pushButton_next_clicked();

      void on_pushButton_mute_clicked();

      void on_pushButton_about_clicked();

      void on_pushButton_exit_clicked();

      void on_pushButton_SearchSong_clicked();

      void on_horizontalSlider_Volume_valueChanged(int value);

      //void on_horizontalSlider_PlayProgress_sliderMoved(int position);

     // void on_horizontalSlider_PlayProgress_valueChanged(int value);


      void on_horizontalSlider_PlayProgress_sliderReleased();

private:
      Ui::Widget *ui;
};

#endif // WIDGET_H
